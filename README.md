# Semi81

The [ZX81 Semigraphics characters. In a font](semi81.ttf).

This repo has code that generates the ZX81 semigraphics characters
for a font.

The ZX81 graphics characters are part of Unicode but
often not featured in fonts.

The 22 semigraphics characters from the ZX81 character set
have assigned Unicode code points and appear at those positions.
5 of those are not in the Basic Multilingual Plane (> 0xFFFF).
They, alongwith many others, were added to the Unicode block _Symbols for
Legacy Computing_ in Unicode verion 13.0.
That was published in 2020, so these non-BMP codes should work
okay in 2023.

For your convenience, i have also implemented the
"just sit on ASCII approach", which 
also (mostly) preserves the ordering in the ZX81 character set.

In tabular form:

<table><thead><tr>
<td/>
<td>…0</td><td>…1</td><td>…2</td><td>…3</td><td>…4</td><td>…5</td><td>…6</td><td>…7</td><td>…8</td><td>…9</td><td>…A</td><td>…B</td><td>…C</td><td>…D</td><td>…E</td><td>…F</td>
</tr></thead>
<tr><th> U+002x</th>
<td><img src='img/u0020.png'/></td><td/><td/><td><img src='img/u0023.png'/></td><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/></tr>
<tr><th> U+004x</th>
<td><img src='img/u0040.png'/></td><td><img src='img/u0041.png'/></td><td><img src='img/u0042.png'/></td><td><img src='img/u0043.png'/></td><td><img src='img/u0044.png'/></td><td><img src='img/u0045.png'/></td><td><img src='img/u0046.png'/></td><td><img src='img/u0047.png'/></td><td><img src='img/u0048.png'/></td><td><img src='img/u0049.png'/></td><td><img src='img/u004A.png'/></td><td/><td/><td/><td/><td/></tr>
<tr><th> U+006x</th>
<td><img src='img/u0060.png'/></td><td><img src='img/u0061.png'/></td><td><img src='img/u0062.png'/></td><td><img src='img/u0063.png'/></td><td><img src='img/u0064.png'/></td><td><img src='img/u0065.png'/></td><td><img src='img/u0066.png'/></td><td><img src='img/u0067.png'/></td><td><img src='img/u0068.png'/></td><td><img src='img/u0069.png'/></td><td><img src='img/u006A.png'/></td><td/><td/><td/><td/><td/></tr>
<tr><th> U+258x</th>
<td><img src='img/u2580.png'/></td><td/><td/><td/><td><img src='img/u2584.png'/></td><td/><td/><td/><td><img src='img/u2588.png'/></td><td/><td/><td/><td><img src='img/u258C.png'/></td><td/><td/><td/></tr>
<tr><th> U+259x</th>
<td><img src='img/u2590.png'/></td><td/><td><img src='img/u2592.png'/></td><td/><td/><td/><td><img src='img/u2596.png'/></td><td><img src='img/u2597.png'/></td><td><img src='img/u2598.png'/></td><td><img src='img/u2599.png'/></td><td><img src='img/u259A.png'/></td><td><img src='img/u259B.png'/></td><td><img src='img/u259C.png'/></td><td><img src='img/u259D.png'/></td><td><img src='img/u259E.png'/></td><td><img src='img/u259F.png'/></td></tr>
<tr><th>U+1FB8x</th>
<td/><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/><td><img src='img/u1FB8E.png'/></td><td><img src='img/u1FB8F.png'/></td></tr>
<tr><th>U+1FB9x</th>
<td><img src='img/u1FB90.png'/></td><td><img src='img/u1FB91.png'/></td><td><img src='img/u1FB92.png'/></td><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/></tr>
</table>

- U+0020 /space the space
- U+0023 /numbersign inverse video space
- U+0060 to U+0067 /grave and /a through /g the 8 quarter-pixel 2×2 blocks,
  in the same order they appear in the ZX81 character set.
- U+0068 to U+006A /h /i /j the 50% dither block and the two halves
- U+0040 to U+004A /at and /A through /J, the reverse video of
  /grave and /a through /j


## Prerequisites

- Go
- Font 8


## Compiling

The whole process is run with

    sh mk.sh

This roughly runs the steps:

- `semi81` the Go program is compiled;
- `png` subdirectory is populated with PNG files by running `semi81`;
  one PNG file per character;
- `vec8` and `ttf8` and other Font 8 tools are used to
  make the `semi81.ttf` file.

The PNG image files are black and white:

- black background (field)
- white foreground (figure)

This is reverse video compared to how the ZX81 would display
these, but is required for `vec8` from Font 8.

# END
