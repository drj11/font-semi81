# TODO

Consider extending to all characters in
[Symbols for Legacy
Computing](https://en.wikipedia.org/wiki/Symbols_for_Legacy_Computing)
block.
Or at least those that are available as 8×8 blocks.

Other fonts in the family could encode 8×13 8×16 or whatever.

Where there are variations, they could be captured in Sylistic
Set features.

Charset PETSCII https://www.pagetable.com/c64ref/charset/ is
just an awesome (and slightly confusing) reference work for the
Commodore line of PET/VIC/C64 microcomputers.
Includes binary downloads of all the fonts (generally in raw
1024 byte ROM-order format)

# END
