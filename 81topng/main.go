package main

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"log"
	"os"
)

// Draw the semigraphics from ZX81 character set,
// output as PNG files.

var b00 = "00000000"
var b0F = "00001111"
var b55 = "01010101"
var bAA = "10101010"
var bF0 = "11110000"
var bFF = "11111111"

// The 0 1 black/white convention is as follows
// 0 = black = background
// 1 = white = foreground
// (suitable for Font 8's `vec8` tool)
//
// The ZX81 has a white background (like print),
// therefore these glyphs, when shown as PNG files, will
// be reversed out compared to their appearance on the ZX81.
//
// Refer to https://en.wikipedia.org/wiki/ZX81_character_set
// for character set.
// Tiles are presented here in order of the ZX81 character set:
// first the 8 half-pixel designs (they are encoded in binary
// order, with bits 0 1 2 being half-pixels NW NE SW), then
// the 3 dither-shade (full, south, north),
// then their reverse video forms.
// In the ZX81 ROM encoding, the first 11 are character codes
// 0 through 10, and the reverse video forms have the high bit
// set and so are coded at 128 through 138.
//
var tiles = map[rune][]string{
	// ZX81 codes 0 to 10
	0x0020:   {b00, b00, b00, b00, b00, b00, b00, b00},
	0x2598:   {bF0, bF0, bF0, bF0, b00, b00, b00, b00}, // NW
	0x259D:   {b0F, b0F, b0F, b0F, b00, b00, b00, b00}, // NE
	0x2580:   {bFF, bFF, bFF, bFF, b00, b00, b00, b00}, // N
	0x2596:   {b00, b00, b00, b00, bF0, bF0, bF0, bF0}, // SW
	0x258C:   {bF0, bF0, bF0, bF0, bF0, bF0, bF0, bF0}, // W
	0x259E:   {b0F, b0F, b0F, b0F, bF0, bF0, bF0, bF0}, // SW-NE
	0x259B:   {bFF, bFF, bFF, bFF, bF0, bF0, bF0, bF0}, // r
	0x2592:   {bAA, b55, bAA, b55, bAA, b55, bAA, b55},
	0x01FB8F: {b00, b00, b00, b00, bAA, b55, bAA, b55},
	0x01FB8E: {bAA, b55, bAA, b55, b00, b00, b00, b00},

	// The above, reversed out; codes 128 and following.
	0x2588:   {bFF, bFF, bFF, bFF, bFF, bFF, bFF, bFF},
	0x259F:   {b0F, b0F, b0F, b0F, bFF, bFF, bFF, bFF}, // L-mirrored
	0x2599:   {bF0, bF0, bF0, bF0, bFF, bFF, bFF, bFF}, // L
	0x2584:   {b00, b00, b00, b00, bFF, bFF, bFF, bFF}, // S
	0x259C:   {bFF, bFF, bFF, bFF, b0F, b0F, b0F, b0F}, // r-mirrored
	0x2590:   {b0F, b0F, b0F, b0F, b0F, b0F, b0F, b0F}, // E
	0x259A:   {bF0, bF0, bF0, bF0, b0F, b0F, b0F, b0F}, // NW-SE
	0x2597:   {b00, b00, b00, b00, b0F, b0F, b0F, b0F}, // SE
	0x01FB90: {b55, bAA, b55, bAA, b55, bAA, b55, bAA},
	0x01FB91: {bFF, bFF, bFF, bFF, b55, bAA, b55, bAA},
	0x01FB92: {b55, bAA, b55, bAA, bFF, bFF, bFF, bFF},
}

func main() {
	for k, v := range tiles {
		img := imgFromTile(v)
		// Note, uppcase hex required for Adobe Glyph List
		// compatibility:
		// https://github.com/adobe-type-tools/agl-specification#2-the-mapping
		path := fmt.Sprintf("u%04X.png", k)
		w, err := os.OpenFile(path, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0644)
		if err != nil {
			log.Print(err)
			continue
		}
		png.Encode(w, img)
		w.Close()
	}
}

func imgFromTile(s []string) image.Image {
	img := image.NewGray(image.Rect(0, 0, 8, 8))
	for y, row := range s {
		for x, p := range row {
			g := uint8(0)
			if p == '1' {
				g = 255
			}
			img.SetGray(x, y, color.Gray{g})
		}
	}
	return img
}
